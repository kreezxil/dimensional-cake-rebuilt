[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## **Description**

Inspired heavily by the Dimensional Cake, this mod aims to have delicious food-themed transport. Cakes for everywhere.

Right now, though, it just has the End Cake.

**End Cake**

Crafted with Eyes of Ender and a cake, filled with Eyes of Ender. Takes you on a one-way trip to the End. Unless you brought two cakes...

![](https://media.forgecdn.net/attachments/224/132/3a32754fd4d2671953a7ed3f903f42c1.png)

## History

Dimensional Cake which was last seen for 1.9.4 and MIT Licensed can be found at [https://minecraft.curseforge.com/projects/dimensional-cake](https://minecraft.curseforge.com/projects/dimensional-cake), this mod uses the textures of that mod. The code has been redesigned from the ground up.

## Changes

*   Models actually work vs the 1.9.4 instance of Dimensional Cake
*   Removed the unnecessary config; you don't have to be hungry, the cake is always useable, edible.
*   Whether or not it was an issue in the other mod, returning from the end without having killed THE ENDER DRAGON doesn't allow the dragon's hit bar to remain on the screen

## Plans

*   back port to 1.11.2 and 1.10.2
*   add nether cake
*   add cake for custom defined dimensions
*   once more cake is added, back porting will commence as deep as my knowledge permits

## Modpacks

Don't ask, just add, a link back to this page is not required but would be most enjoyable.

## Reviewers

Let me know in the comment section below and I'll add your mod review video to the overview for prominent viewing to all.

## Help a Veteran Today

I am a US Veteran. I am not disabled but I do spend a lot of time making this mods and modpacks for you. Any funding you can spare to help me to continue doing that is greatly appreciated. Please donate at [https://patreon.com/kreezxil](https://patreon.com/kreezxil) .

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
